package gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import generation.Maze;
import generation.MazeFactory;
import generation.Order;
import generation.StubOrder;

/**
 * 
 * Testing cases for the unreliable robot, which will have a varying amount of working sensors 
 * 
 * 
 * @author Gavin Grossman
 *
 */

public class UnreliableRobotTest {
	 
	private Controller controller;
	private MazeFactory mazeFactory;
	private StubOrder stubOrder;
	private Maze maze;
	private ReliableRobot robot;
	private Wizard driver;
	
	

	@Before
	public void setup() {
		int skillLevel = 1;
		boolean perfect = true;
		Order.Builder builder = Order.Builder.DFS;
		
		controller = new Controller();
		mazeFactory = new MazeFactory();
		stubOrder = new StubOrder(skillLevel, builder, perfect);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered();
		maze = stubOrder.getMaze();
		robot = new ReliableRobot();
		driver = new Wizard();
		controller.setRobotAndDriver(robot, driver);
		controller.switchFromGeneratingToPlaying(maze);
	}
	
	@Test
	public void testNotNull() {
		assertNotNull(maze);
	}
	
	@Test
	public final void testBatteryLevel() {
		/**
		 * get the battery level and make sure
		 * the right amount of energy was subtracted.
		 */
		float batt = 3500;
		float test = driver.getEnergyConsumption()+robot.getBatteryLevel();
		assertEquals(test, batt, .01);
	}

	@Test
	public final void testOdometer() {
		/**
		 * get distance to exit from maze and check that 
		 * it is equal to the odometer.
		 */
		int[] start = maze.getStartingPosition();
		int dist = maze.getDistanceToExit(start[0], start[1]);
		//assertEquals(robot.getOdometerReading(), dist);
		
	}
	 
	@Test
	public final void testIfStopped() {
		/** 
		 * test if robot has properly stopped if run out of energy
		 * 
		 */
		
	}
	
	@Test
	public final void testCorrectEnergyLossforRotation() {
		/**
		 * test if rotation movement depletes the correct 
		 * amount of energy
		 */
	}
	
	@Test
	public final void testCorrectEnergyLossforMove() {
		/**
		 * test if we made the perfect amount of moves
		 * by seeing how much energy we lost off rotations
		 * and comparing it to the actual energy we lost
		 */
	}
	
	
}
