package gui;

import generation.Maze;
import generation.MazeFactory;
import generation.Order;
import generation.StubOrder;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


/**
 * 
 * Testing cases for the robot driver Wizard which will 
 * follow the shortest path to the exit using neighbors.
 * 
 * 
 * @author Gavin Grossman
 *
 */

public class WizardTest {
	 
	private Controller controller;
	private MazeFactory mazeFactory;
	private StubOrder stubOrder;
	private Maze maze;
	private ReliableRobot robot;
	private Wizard driver;
	
	@Before
	public void setup() {
		int skillLevel = 1;
		boolean perfect = true;
		Order.Builder builder = Order.Builder.DFS;
		
		controller = new Controller();
		mazeFactory = new MazeFactory();
		stubOrder = new StubOrder(skillLevel, builder, perfect);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered();
		maze = stubOrder.getMaze();
		robot = new ReliableRobot();
		driver = new Wizard();
		controller.setRobotAndDriver(robot, driver);
		controller.switchFromGeneratingToPlaying(maze);
	}
	
	@Test
	public void testNotNull() {
		/**
		 * tests if maze is instantiated
		 */
		assertNotNull(maze);
	}
	
	@Test
	public final void testInitialEnergy() {
		/**
		 * tests if getEnergyConsumption and getBatterLevel
		 * are both working properly.
		 */
		float batt = 3500;
		float test = driver.getEnergyConsumption()+robot.getBatteryLevel();
		assertEquals(test, batt, .01);
	}

	@Test
	public final void testPathLength() {
		/**
		 * tests of pathlength is equal to the shortest distance
		 * we get from maze.distance to exit method.
		 */
		int[] start = maze.getStartingPosition();
		int dist = maze.getDistanceToExit(start[0], start[1]);
		//assertEquals(dist, driver.getPathLength());
	}
	 
	@Test
	public final void testDrive2Exit() throws Exception {
		/**
		 * tests if driver makes it to exit without running into a wall or
		 * running out of battery.
		 */
		//assertEquals(driver.drive2Exit(), false);
	}
	
}
