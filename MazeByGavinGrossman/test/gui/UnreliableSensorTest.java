package gui;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import generation.Maze;
import generation.MazeFactory;
import generation.Order;
import generation.StubOrder;

/**
 * Testing class for the sensors that the robot uses.
 * The unreliable sensors will alternate between working and not.
 * 
 * @author Gavin Grossman
 *
 */

public class UnreliableSensorTest {
	 
	private Controller controller;
	private MazeFactory mazeFactory;
	private StubOrder stubOrder;
	private Maze maze;
	private ReliableRobot robot;
	private Wizard driver;

	@Before
	public void setup() {
		int skillLevel = 1;
		boolean perfect = true;
		Order.Builder builder = Order.Builder.DFS;
		
		controller = new Controller();
		mazeFactory = new MazeFactory();
		stubOrder = new StubOrder(skillLevel, builder, perfect);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered();
		maze = stubOrder.getMaze();
		robot = new ReliableRobot();
		driver = new Wizard();
		controller.setRobotAndDriver(robot, driver);
		controller.switchFromGeneratingToPlaying(maze);
	}
	
	@Test
	public void testNotNull() {
		assertNotNull(maze);
	}
	
	@Test
	public final void testDistToObstacle() {
		/**
		 * check that there is a wall the distance that this method returns.
		 * use the floorplan of the maze to check that there is a wall
		 * in either x+i or y+i positions
		 */
		
	}

	@Test
	public final void testSensorEnergy() {
		/**
		 * check that the sensor correctly subtracted one from the energy each time
		 * it used the ability to sense.
		 */
	}
	 
	@Test
	public final void testSensorDirection() {
		/**
		 * check that the sensor is actually mounted in the direction it says
		 */
		
	}
	
}
