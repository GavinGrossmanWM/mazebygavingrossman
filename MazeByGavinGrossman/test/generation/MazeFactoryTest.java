package generation;

import static org.junit.Assert.assertEquals;



import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


/**
 * 
 * @author Gavin Grossman
 *
 */

/**
 * NOTES
 * 
 * Tests methods of the MazeBuilder class.
 * Blackbox tests that work on any maze.
 * 
 * Properties of correct maze: 
 * 	-exit reachable from anywhere
 *	-exit exists
 *	-number walls for perfect maze
 *	-limits on distances
 * 
 * STEPS
 * 	-create MazeFactory
 * 	-create Order
 * 	-use order method of factory
 * 	-use wait method of factory
 * 	-use get method to obtain maze from order
 * 
 */

public class MazeFactoryTest {
	 
	private MazeFactory mazeFactory;
	private StubOrder stubOrder;
	private Maze maze;
	

	@Before
	public void setup() {
		
		int skillLevel = 3;
		boolean perfect = false;
		Order.Builder builder = Order.Builder.Eller;
		
		mazeFactory = new MazeFactory();
		stubOrder = new StubOrder(skillLevel, builder, perfect);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered();
		maze = stubOrder.getMaze();
	}
	
	@Test
	public void testNotNull() {
		assertNotNull(maze);
	}
	
	@Test
	public final void testExitExists() {
		/**
		 * loop over all outside border (load-bearing) walls.
		 * if one is missing, add to counter.
		 * if counter != 1:
		 * 	test failed
		 * 
		 */
		int count = 0;
		for (int i = 0; i < maze.getWidth(); i++) {
			if (!maze.hasWall(i, 0, CardinalDirection.North)) {
				count += 1;
			}
		}
		for (int i = 0; i < maze.getHeight(); i++) {
			if (!maze.hasWall(0, i, CardinalDirection.West)) {
				count += 1;
			}
		}
		for (int i = 0; i < maze.getWidth(); i++) {
			if (!maze.hasWall(i, maze.getHeight() - 1, CardinalDirection.South)) {
				count += 1;
			}
		}
		for (int i = 0; i < maze.getHeight(); i++) {
			if (!maze.hasWall(maze.getWidth()-1, i, CardinalDirection.East)) {
				count += 1;
			}
		}
		assertEquals(1, count);
	}

	@Test
	public final void testExitAccess() {
		/**
		 * loop over all cells in maze.
		 * if we find a cell where mazedists is not computable:
		 * 	test failed
		 */
		// look at MazeContainer.mazedists distance matrix
		boolean check = true;
		for(int i=0; i<maze.getWidth(); i++) {
	        for(int j=0; j<maze.getHeight(); j++) {
	        	if (maze.hasWall(i, j, CardinalDirection.North) && maze.hasWall(i, j, CardinalDirection.East) && maze.hasWall(i, j, CardinalDirection.South) && maze.hasWall(i, j, CardinalDirection.West)) {
	        		check = false;
	        	}
				assertEquals(check, true);
	        }
		}
	}
	 
	@Test
	public final void testExitDistance() {
		/**
		 * get distances from each cell to the exit.
		 * if any cell's dist larger than the starting spot:
		 * 	test failed
		 */
		int[] start = maze.getStartingPosition();
		Distance dist = maze.getMazedists();
		int[][] dists = dist.getAllDistanceValues();
		int startdist = dist.getDistanceValue(start[0], start[1]);
		for(int i=0; i<dists.length; i++) {
	        for(int j=0; j<dists[i].length; j++) {
				assertFalse(startdist < dists[i][j]);
	        }
		}
		
	}
	
	@Test
	public final void testNumberWallsPerfect() {
		/**
		 * number of walls should equal:
		 * 	(number of cells + width + height)
		 * if not:
		 * 	not perfect maze
		 *
		 */
		int perfectwalls = (maze.getHeight()*maze.getWidth()+maze.getHeight()+maze.getWidth());
		//int num = MazeBuilderEller.getNumWalls();
		// DIDN'T FIND A METHOD TO GET NUMBER OF WALLS IN MAZE..
		
	}
	
	@Test
	public final void testBounds() {
		/**
		 * distance cannot be < 1.
		 * distance cannot be > number of cells.
		 * if outside bounds:
		 * 	test failed
		 */
		Distance dist = maze.getMazedists();
		int[][] dists = dist.getAllDistanceValues();
		for(int i=0; i<dists.length; i++) {
	        for(int j=0; j<dists[i].length; j++) {
				assertTrue(dists[i][j] > 0);
				assertTrue(dists[i][j] < (maze.getHeight()*maze.getWidth()+1));
	        }
	}
	}
	
}
