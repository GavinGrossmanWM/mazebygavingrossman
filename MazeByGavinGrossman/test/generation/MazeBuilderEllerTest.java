package generation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class MazeBuilderEllerTest  {

	private MazeFactory mazeFactory;
	private StubOrder stubOrder;
	private Maze maze;
	private Floorplan floorplan;
	

	@Before
	public void setup() {
		
		int skillLevel = 3;
		boolean perfect = false;
		Order.Builder builder = Order.Builder.Eller;
		
		mazeFactory = new MazeFactory();
		stubOrder = new StubOrder(skillLevel, builder, perfect);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered();
		maze = stubOrder.getMaze();
		floorplan = maze.getFloorplan();
		
	}
	
	@Test
	public void testNotNull() {
		assertNotNull(maze);
	}
	
	@Test 
	public void testLastRow() {
		/* 
		 * want to check that last row does not
		 * have any cells that cannot reach the exit.
		 * loop over cells (0->width-1, height-1):
		 * 	check distances to exit are computable
		 */
		boolean check = true;
		int y = maze.getHeight() - 1;
		for(int x=0; x<maze.getWidth(); x++) {
			if (maze.getDistanceToExit(x, y) == Distance.INFINITY) {
				check = false;
			}
		}
		assertEquals(check, true);
	}
	
	@Test
	public void testHorizontalRoomWall() {
		/*
		 * if we get to the right edge of a room and the floor is unbreakable
		 * we need to make sure that the set already has a door to the row below.
		 * loop until we find a case where there is a wall to the right and
		 * unbreakable wall below :
		 * and go backwards through set to check for door
		 */
		boolean check = true;
		for(int i=0; i<maze.getHeight(); i++) {
	        for(int j=0; j<maze.getWidth(); j++) {
	        
	        	if (maze.hasWall(j, i, CardinalDirection.East) && maze.hasWall(j, i, CardinalDirection.South) && maze.getDistanceToExit(j, i) == Distance.INFINITY) {
	        		check = false;
	        	}
	        }	        
	    }
		assertEquals(check, true);
		
	}
	
	@Test
	public void testVerticalRoomWall() {
		/*
		 * if we get to a border wall to the right:
		 * make sure either there is no floor below
		 * or some cell in set has a southern door
		 */
		boolean check = true;
		for(int i=0; i<maze.getHeight(); i++) {
	        for(int j=0; j<maze.getWidth(); j++) {
	        	Wallboard wallboard = new Wallboard(j, i, CardinalDirection.East);
	        	Wallboard wallboardsouth = new Wallboard(j, i, CardinalDirection.South);
	        
	        	if (floorplan.isPartOfBorder(wallboard) && floorplan.isPartOfBorder(wallboardsouth) && maze.getDistanceToExit(j, i) == Distance.INFINITY) {
	        		check = false;
	        	}
	        }	        
	    }
		assertEquals(check, true);
	}
	
	@Test
	public void testLastColumn() {
		/*
		 * loop over cells in last column.
		 * make sure there is either no floor or 
		 * it can reach the exit in < width*height moves.
		 */
		boolean check = true;
		int x = maze.getWidth() - 1;
		int yl = maze.getHeight();
	        for(int y=0; y<yl; y++) {
	        	Wallboard wallboard = new Wallboard(x, y, CardinalDirection.East);
	        	Wallboard wallboardsouth = new Wallboard(x, y, CardinalDirection.South);
	        
	        	if (floorplan.isPartOfBorder(wallboard) && floorplan.isPartOfBorder(wallboardsouth) && maze.getDistanceToExit(x, y) > (x+1)*y) {
	        		check = false;
	        	}
	        }	        
	    
		assertEquals(check, true);
	}
}
