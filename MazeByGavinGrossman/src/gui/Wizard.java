package gui;

import generation.Maze;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * The Wizard class specifies a robot driver that operates a robot to escape from a given maze
 * by knowing the distance matrix and thus following the shortest path through the maze. 
 * 
 * Collaborators: reliable robot class
 * 
 * Implements: RobotDriver interface
 * 
 * @author Gavin Grossman
 *
 */

public class Wizard implements RobotDriver{
	
	public Robot robot;
	protected Maze maze;
	protected float initialEnergy;
	protected int pathLength = 0;

	/**
	 * assigns the robot to the driver.
	 * sets initial energy.
	 */
	@Override
	public void setRobot(Robot r) {
		this.robot = r;
		initialEnergy = robot.getBatteryLevel();
	}

	/**
	 * provides maze info for driver.
	 */
	@Override
	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	/**
	 * while robot is not at the exit position;
	 * 	use drive1step to progress the robot and update what we see on screen.
	 * 
	 * when it reaches position of exit, rotate until it faces the exit;
	 * 	then move forward one, triggering controller to finish the game.
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		while (!robot.isAtExit()) {
			if (robot.hasStopped()) {
				return false;
			}
			drive1Step2Exit();
			pathLength++;
		}		
		if (robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)) {
			robot.move(1);
		} else if (robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD)) {
			robot.rotate(Turn.AROUND);
		} else if (robot.canSeeThroughTheExitIntoEternity(Direction.LEFT)) {
			robot.rotate(Turn.LEFT);
		} else if (robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT)) {
			robot.rotate(Turn.RIGHT);
		}
		robot.move(1);
		pathLength++;
		return true;
	}

	/**
	 * uses the maze's getNeighbor method to follow the shortest path.
	 * 
	 * after getting the coordinates of the neighbor:
	 * 	rotate to face neighbor using robot.rotate,
	 * then move one position.
	 * 
	 * add 1 to the pathlength each time.
	 */
	@Override
	public boolean drive1Step2Exit() throws Exception {
		int x = robot.getCurrentPosition()[0];
		int y = robot.getCurrentPosition()[1];
		int neighbor[] = maze.getNeighborCloserToExit(x, y);
		int switcher = 0;
		switch (robot.getCurrentDirection()) {
		case North:
			switcher = 1;
			break;
		case South:
			switcher = 2;
			break;
		case East:
			switcher = 3;
			break;
		case West:
			switcher = 4;
			break;
		}
		if (neighbor[0] > x && neighbor[1] == y) {
			if (switcher == 1) {
				robot.rotate(Turn.LEFT);
			}
			else if (switcher == 2) {
				robot.rotate(Turn.RIGHT);
			}
			else if (switcher == 4) {
				robot.rotate(Turn.AROUND);
				}
			robot.move(1);
		} else if (neighbor[0] == x && neighbor[1] < y) {
			if (switcher == 3) {
				robot.rotate(Turn.RIGHT);
			}
			else if (switcher == 4) {
				robot.rotate(Turn.LEFT);
			}
			else if (switcher == 2) {
				robot.rotate(Turn.AROUND);
				}
			robot.move(1);
		} else if (neighbor[0] == x && neighbor[1] > y) {
			if (switcher == 4) {
				robot.rotate(Turn.RIGHT);
			}
			else if (switcher == 3) {
				robot.rotate(Turn.LEFT);
			}
			else if (switcher == 1) {
				robot.rotate(Turn.AROUND);
				}
			robot.move(1);
		} else if (neighbor[0] < x && neighbor[1] == y) {
			if (switcher == 2) {
				robot.rotate(Turn.LEFT);
			}
			else if (switcher == 1) {
				robot.rotate(Turn.RIGHT);
			}
			else if (switcher == 3) {
				robot.rotate(Turn.AROUND);
				}
			robot.move(1);
		}
		return true;
	}
		
	/**
	 * subtract the initialenergy that we set with the batterylevel that the robot provides.
	 */
	@Override
	public float getEnergyConsumption() {
		return initialEnergy - robot.getBatteryLevel();
	}

	/**
	 * return the length of travel kept in this class
	 */
	@Override
	public int getPathLength() {
		return this.pathLength;
	}

}
