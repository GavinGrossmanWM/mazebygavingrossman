package gui;

import generation.CardinalDirection;

import generation.Maze;
import gui.Constants.UserInput;
import generation.Floorplan;

/**
 * This class creates a robot that will travel through the maze and 
 * update the screen so that we can watch it solve it.
 * 
 * This version, ReliableRobot, will have all of its sensors working. 
 * 
 * Collaborators: Controller and RobotDriver that moves the robot, ReliableSensor
 * 
 * Implements: Robot interface
 * 
 * @author Gavin Grossman
 *
 */

public class ReliableRobot implements Robot {
	
	protected Controller controller;
	protected Maze maze;
	protected Floorplan floorplan;
	protected static final int forward = 6;
	protected static final int rotate = 3;
	protected static final int jump = 40;
	protected int odometer;
	protected int x;
	protected int y;
	protected boolean stopped = false;
	protected float batteryLevel;
	
	protected DistanceSensor rightsensor = new ReliableSensor();
	protected DistanceSensor leftsensor = new ReliableSensor();
	protected DistanceSensor forwardsensor = new ReliableSensor();
	protected DistanceSensor backwardsensor = new ReliableSensor();

	/**
	 * private method that initializes all 4 of its sensors
	 */
	protected void setSensors() {
		rightsensor.setSensorDirection(Direction.RIGHT);
		rightsensor.setMaze(maze);
		leftsensor.setSensorDirection(Direction.LEFT);
		leftsensor.setMaze(maze);
		forwardsensor.setSensorDirection(Direction.FORWARD);
		forwardsensor.setMaze(maze);
		backwardsensor.setSensorDirection(Direction.BACKWARD);
		backwardsensor.setMaze(maze);
	}

	/**
	 * gets controller, maze, and floorplan to operate on.
	 * also sets batterylevel and sensors.
	 */
	@Override
	public void setController(Controller controller) {
		this.controller = controller;
		this.maze = controller.getMazeConfiguration();
		this.floorplan = maze.getFloorplan();
		setBatteryLevel(3500);
		setSensors();
	}

	/**
	 * returns current position using controller
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {
		int position[] = controller.getCurrentPosition();
		x = position[0];
		y = position[1];
		return position;
	}

	/**
	 * returns current direction using controller
	 */
	@Override
	public CardinalDirection getCurrentDirection() {
		CardinalDirection curDirection = controller.getCurrentDirection();
		return curDirection;
	}

	/**
	 * returns battery level
	 */
	@Override
	public float getBatteryLevel() {
		return this.batteryLevel;
	}

	/**
	 * sets battery level
	 */
	@Override
	public void setBatteryLevel(float level) {
		this.batteryLevel = level;	
	}

	/**
	 * energy used for 360 degree rotation
	 */
	@Override
	public float getEnergyForFullRotation() {
		return (4*rotate);
	}

	/**
	 * returns how much energy it takes to move
	 */
	@Override
	public float getEnergyForStepForward() {
		return forward;
	}

	/**
	 * returns how far we've gone
	 */
	@Override
	public int getOdometerReading() {
		return this.odometer;
	}

	/**
	 * resets odometer to 0
	 */
	@Override
	public void resetOdometer() {
		this.odometer = 0;	
	}

	/**
	 * turn view of robot using the controller.
	 * takes as input which way to turn.
	 * use switch statement and controller.keyDown method
	 * subtract rotation cost from battery
	 */
	@Override
	public void rotate(Turn turn) {
		switch(turn) {
		case RIGHT:
			controller.keyDown(UserInput.Right, 0);
			break;
		case LEFT:
			controller.keyDown(UserInput.Left, 0);
			break;
		case AROUND:
			controller.keyDown(UserInput.Right, 0);
			controller.keyDown(UserInput.Right, 0);
			batteryLevel -= rotate;
		}
		batteryLevel -= rotate;
	}

	/**
	 * moves robot a certain distance as specified by parameter.
	 * while we haven't run into a wall or lost power,
	 * move forward the correct number of spaces.
	 * subtract distance from battery level and increase odometer.
	 */
	@Override
	public void move(int distance) {
		while (!hasStopped() && (distance != 0)){
			if ((distanceToObstacle(Direction.FORWARD) == 0) || (batteryLevel < forward)){
				stopped = true;
				move(1);
			} else { 
				controller.keyDown(UserInput.Up, 0);
				odometer++;
				distance--;
				batteryLevel -= forward;
			}
		}
	}


	/**
	 * move forward one, even through wall if battery level allows
	 */
	@Override
	public void jump() {
		if (batteryLevel < forward) {
			stopped = true;
		} else {
			controller.keyDown(UserInput.Jump, 0);
			odometer++;
			batteryLevel -= jump;
		}
	}

	/**
	 *returns true if distance to exit is one,
	 *false otherwise
	 */
	@Override
	public boolean isAtExit() {
		try {
			int[] position = getCurrentPosition();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (maze.getDistanceToExit(x, y) == 1) {
			return true;
		}
		return false;
	}

	/**
	 * use floorplan to see if we are in a room
	 */
	@Override
	public boolean isInsideRoom() {
		if (floorplan.isInRoom(x, y)) {
			return true;
		}
		return false;
	}

	/**
	 * return true if we run out of power or hit wall
	 */
	@Override
	public boolean hasStopped() {
		if (batteryLevel <= 0 || stopped == true) {
			return true;
		}
		return false;
	}

	/**
	 * call on the reliable sensor to tell us how far it is to the wall
	 * in the parameter direction.
	 * use switch statements for each of the four directions and 
	 * call the corresponding method in Reliable sensor.
	 * return the distance.
	 */
	@Override
	public int distanceToObstacle(Direction direction) {
		DistanceSensor distsensor = null;
		switch (direction) {
		case FORWARD:
			distsensor = forwardsensor;
			break;
		case BACKWARD:
			distsensor = backwardsensor;
			break;
		case LEFT:
			distsensor = leftsensor;
			break;
		case RIGHT:
			distsensor = rightsensor;
			break;
		}
		float[] power;
		power = new float[1];
		power[0] = batteryLevel;
		int dist = Integer.MAX_VALUE;
		try {
			dist = distsensor.distanceToObstacle(getCurrentPosition(), getCurrentDirection(), power);
		} catch (Exception e) {
			throw new UnsupportedOperationException("The robot cannot see in that direction");
		}
		batteryLevel = power[0];
		return dist;

	}

	/**
	 * return true if the distance to a wall is infinity
	 */
	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		return (distanceToObstacle(direction) == Integer.MAX_VALUE);
	}

	/**
	 * will be used in unreliablerobot
	 */
	@Override
	public void startFailureAndRepairProcess(Direction direction, int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		throw new UnsupportedOperationException();	
	}
	
	/**
	 * will be used in unreliablerobot
	 */
	@Override
	public void stopFailureAndRepairProcess(Direction direction) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();		
	}

}
