package gui;

/**
 * 4 distance sensors. Each sensor has its own and independent failure and repair process
 * mean time to repair (down time) is 2 seconds,
 * mean time between failures (up time) is 4 seconds.
 * The Controller (or StatePlaying) should pick up the responsibility to decide which sensors
 * are configured to have a running failure & repair process and to start these processes.
 * 
 * Collaborators: Controller and robot driver that moves the robot
 * 
 * Implements: Robot
 * 
 * Extends: ReliableRobot
 * 
 * @author Gavin Grossman
 *
 */
public class UnreliableRobot extends ReliableRobot implements Robot {
	
	/**
	 * set the sensor to an unreliable one based on the direction.
	 * start the failure and repair process.
	 */
	@Override
	public void startFailureAndRepairProcess(Direction direction, int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		switch (direction) {
		case FORWARD:
			forwardsensor = new UnreliableSensor();
			forwardsensor.setMaze(maze);
			forwardsensor.setSensorDirection(direction);
			forwardsensor.startFailureAndRepairProcess(meanTimeBetweenFailures, meanTimeToRepair);
			break;
		case BACKWARD:
			backwardsensor = new UnreliableSensor();
			backwardsensor.setMaze(maze);
			backwardsensor.setSensorDirection(direction);
			backwardsensor.startFailureAndRepairProcess(meanTimeBetweenFailures, meanTimeToRepair);
			break;
		case LEFT:
			leftsensor = new UnreliableSensor();
			leftsensor.setMaze(maze);
			leftsensor.setSensorDirection(direction);
			leftsensor.startFailureAndRepairProcess(meanTimeBetweenFailures, meanTimeToRepair);
			break;
		case RIGHT:
			rightsensor = new UnreliableSensor();
			rightsensor.setMaze(maze);
			rightsensor.setSensorDirection(direction);
			rightsensor.startFailureAndRepairProcess(meanTimeBetweenFailures, meanTimeToRepair);
			break;
		}
	
	}

	/**
	 * stop the failure and repair process for the sensor for the given direction
	 */
	@Override
	public void stopFailureAndRepairProcess(Direction direction) throws UnsupportedOperationException {
		switch (direction) {
		case FORWARD:
			forwardsensor.stopFailureAndRepairProcess();
			break;
		case BACKWARD:
			backwardsensor.stopFailureAndRepairProcess();
			break;
		case LEFT:
			leftsensor.stopFailureAndRepairProcess();
			break;
		case RIGHT:
			rightsensor.stopFailureAndRepairProcess();
			break;
		}	
	}

}
