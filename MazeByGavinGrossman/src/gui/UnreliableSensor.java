package gui;


/**
 * extended version of the
ReliableSensor that allows for failure and repair events. It needs to fully support a method to
start and perform a failure and repair process as well as a method to stop it.
1. You are challenged to implement this behavior with an individual and separate thread,
one per sensor.
2. The sensor’s background thread flips the sensor’s internal operational state between
operational and failed. It is fair to assume for this assignment that the mean time to
repair and mean time between failures are constant times (no need to work with actual
distributions).
3. The duration for being in a failed state is given by the meanTimeToRepair, which is
considered constant and fixed to 2 seconds for this assignment.
4. The duration for being in an operational state is given by the
meaneanTimeBetweenFailures, which is considered constant and fixed to 4 seconds for
this assignment.
5. The sensor is supposed to start in an operational state at the beginning.
6. The sensor is supposed to be in an operational state after the failure & repair process is
stopped.
7. For an unreliable sensor, the failure & repair process is started right when the game
plays (the driver starts), the process is only stopped when the game switches to the
finish screen.
8. If there are multiple unreliable sensors, there is a 1.3 second delay between starting
their failure & repair processes, i.e., no two processes should start immediately after one
another with no delay in between.
 *
 *	Collaborators: robot and robot driver
 *
 *	Implements: distanceSensor and Runnable
 *
 *	Extends: ReliableSensor
 *
 * @author Gavin Grossman
 *
 */

public class UnreliableSensor extends ReliableSensor implements DistanceSensor, Runnable {
	/**
	 * use 1 background thread for 1 distance sensor. 
	 * So your robot has 4 sensors, and each sensor runs its own background thread. 
	 * So it will be 4 instances of an UnreliableSensor and each will operate its own internal thread (so 4 threads in total)
	 */
	protected boolean isActive = false;
	protected Thread sensorThread;

	/**
	 * private method to trigger the thread to stop 
	 */
	private void toggleInterrupt(){
		isActive = !isActive;
	}

	/**
	 * start a thread with the object itself as the runnable
	 */
	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		sensorThread = new Thread(this);
		sensorThread.start();
		
	}

	/**
	 * stops the thread from running
	 */
	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {
		toggleInterrupt();
	}

	/**
	 * make the thread operational for 4 seconds, and down for 2 seconds
	 */
	@Override
	public void run() {
		while(isActive) {
			try {
				operational = false;
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			try {
				operational = true;
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
		}
	}
}
