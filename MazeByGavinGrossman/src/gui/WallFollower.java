package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * The WallFollower class specifies a robot driver that operates a robot to escape from a given maze
 * by  following the the left hand wall until the exit is visible. 
 * 
 * Collaborators: reliable robot class
 * 
 * Implements: RobotDriver interface
 * 
 * Extends: Wizard
 * 
 * @author Gavin Grossman
 *
 */

public class WallFollower extends Wizard implements RobotDriver {
	
	/**
	 * only need to change the drive1step method as all the others will be the same.
	 * 
	 * follow the left wall using sensors that may or may not be operational:
	 * if the latter, either adjust orientation or wait for them to work.
	 * 
	 * 
	 * NOTE: Ran out of time to try to catch exceptions ):
	 */
			
	@Override
	public boolean drive1Step2Exit() throws Exception {
		if (robot.isInsideRoom() && (robot.distanceToObstacle(Direction.LEFT) != 0)) {
			if (robot.distanceToObstacle(Direction.RIGHT) == 0) {
				robot.rotate(Turn.AROUND);
			}
			else {
				if (robot.distanceToObstacle(Direction.FORWARD) != 0) {
					robot.move(1);
				}	
			}
		}
		else if (!robot.isAtExit()){
			if (((robot.distanceToObstacle(Direction.LEFT) == 0) && 
					(robot.distanceToObstacle(Direction.FORWARD) == 0)) && (robot.distanceToObstacle(Direction.RIGHT) == 0)){
				robot.rotate(Turn.AROUND);
			}
			if (((robot.distanceToObstacle(Direction.LEFT) == 0) && 
					(robot.distanceToObstacle(Direction.FORWARD) == 0)) && (robot.distanceToObstacle(Direction.RIGHT) == 0)){
				robot.rotate(Turn.AROUND);
			}
			else if ((robot.distanceToObstacle(Direction.LEFT) == 0) && (robot.distanceToObstacle(Direction.FORWARD) == 0)){
				robot.rotate(Turn.RIGHT);
			}
			else if (robot.distanceToObstacle(Direction.LEFT) != 0){
				robot.rotate(Turn.LEFT);
			}
			robot.move(1);
		}
		else if ((robot.isAtExit()) && !(robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD))){
			robot.rotate(Turn.LEFT);
		}
		else {
			robot.move(1);
		}
		return true;
	}

}

	
