package gui;

import generation.CardinalDirection;

import generation.Maze;
import gui.Robot.Direction;

/**
 * This class uses the sensors on the robot to get the distance to a wall
 * in the specified direction. It also has methods to account for sensor failure and repair. 
 * 
 * Collaborators: wizard and reliableRobot
 * 
 * Implements: DistanceSensor
 * 
 * @author Gavin Grossman
 *
 */

public class ReliableSensor implements DistanceSensor{
	
	protected Maze maze;
	protected Direction sensorDirection;
	protected int sensorEnergy = 0;
	protected boolean operational = true;

	/**
	 * uses parameters to sense a wall in the given direction.
	 * must take the direction we wish to move and figure out which 
	 * direction on the robot that is.
	 * using a int to hold the orientation:
	 * 	travel through maze until we reach a wall.
	 * 	return distance that it traveled before reaching it.
	 */
	@Override
	public int distanceToObstacle(int[] currentPosition, CardinalDirection currentDirection, float[] powersupply) throws Exception {
		if (operational == false) {
			throw new Exception("sensor down");
		}
		int x = currentPosition[0];
		int y = currentPosition[1];
		CardinalDirection d = currentDirection;
		int switcher = returnSwitcher(d);
		sensorEnergy++;
		powersupply[0] = powersupply[0] - 1;
		return(returnDistance(x, y, switcher));
	}

	/**
	 * private method that returns distance 
	 * @param x
	 * @param y
	 * @param switcher
	 * @return
	 */
	private int returnDistance(int x, int y, int switcher) {
		int distance = Integer.MAX_VALUE;
		switch (switcher) {
		case 3:
			for (int i = 0; i < maze.getDistanceToExit(x, y); i++) {
				if (maze.hasWall(x, y-i, CardinalDirection.North)) {
					distance = i;
					break;
				}
			}
			break;
		case 4:
			for (int i = 0; i < maze.getDistanceToExit(x, y); i++) {
				if (maze.hasWall(x, y+i, CardinalDirection.South)) {
					distance = i;
					break;
				}
			}
			break;
		case 1:
			for (int i = 0; i < maze.getDistanceToExit(x, y); i++) {
				if (maze.hasWall(x-i, y, CardinalDirection.West)) {
					distance = i;
					break;
				}
			}
			break;
		case 2:
			for (int i = 0; i < maze.getDistanceToExit(x, y); i++) {
				if (maze.hasWall(x+i, y, CardinalDirection.East)) {
					distance = i;
					break;
				}
			}
			break;
		}
		return distance;
	}
	
	/**
	 * private method that helps distanceToObtacle method
	 * @param d
	 * @return
	 */
	private int returnSwitcher(CardinalDirection d) {
		int switcher = 0;
		switch (sensorDirection){
		case BACKWARD:
			if (d == CardinalDirection.North) {
				switcher = 4;
			} else if (d == CardinalDirection.South) {
				switcher = 3;
			} else if (d == CardinalDirection.East) {
				switcher = 2;
			} else if (d == CardinalDirection.West) {
				switcher = 1;
			} 
			break;
		case RIGHT:
			if (d == CardinalDirection.North) {
				switcher = 1;
			} else if (d == CardinalDirection.South) {
				switcher = 2;
			} else if (d == CardinalDirection.East) {
				switcher = 3;
			} else if (d == CardinalDirection.West) {
				switcher = 4;
			} 
			break;
		case FORWARD:
			if (d == CardinalDirection.North) {
				switcher = 3;
			} else if (d == CardinalDirection.South) {
				switcher = 4;
			} else if (d == CardinalDirection.East) {
				switcher = 2;
			} else if (d == CardinalDirection.West) {
				switcher = 1;
			} 
			break;
		case LEFT:
			if (d == CardinalDirection.North) {
				switcher = 2;
			} else if (d == CardinalDirection.South) {
				switcher = 1;
			} else if (d == CardinalDirection.East) {
				switcher = 4;
			} else if (d == CardinalDirection.West) {
				switcher = 3;
			} 
			break;
		}
		return switcher;
	}
	
	/**
	 * gets maze to operate on
	 */
	@Override
	public void setMaze(Maze maze) {
		this.maze = maze;		
	}

	/**
	 * gets sensor direction it will be checking
	 */
	@Override
	public void setSensorDirection(Direction mountedDirection) {
		this.sensorDirection = mountedDirection;		
	}

	/**
	 * returns how much energy it took to sense 
	 */
	@Override
	public float getEnergyConsumptionForSensing() {
		return sensorEnergy;
	}

	/**
	 * implemented in unreliableSensor
	 */
	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		throw new UnsupportedOperationException();		
	}

	/**
	 * implemented in unreliableSensor
	 */
	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();	
	}

}
