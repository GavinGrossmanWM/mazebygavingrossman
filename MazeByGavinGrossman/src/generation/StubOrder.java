package generation;

public class StubOrder implements Order {
	
	private int level;
	private int seed;
	private int progress;
	private Order.Builder builder;
	private boolean perfect;
	private Maze Maze;
	
	public StubOrder(int level, Order.Builder builder, boolean perfect) {
		setSkillLevel(level);
		setBuilder(builder);
		setPerfect(perfect);
		
	}

	public void setSkillLevel(int level) {
		this.level = level;
	}
	
	@Override
	public int getSkillLevel() {
		return this.level;
	}
	
	public void setBuilder(Builder builder) {
		this.builder = builder;
	}

	@Override
	public Builder getBuilder() {
		return this.builder;
	}

	public void setPerfect(boolean perfect) {
		this.perfect = perfect;
	}
	
	@Override
	public boolean isPerfect() {
		return this.perfect;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}
	
	@Override
	public int getSeed() {
		return this.seed;
	}

	public Maze getMaze() {
		return this.Maze;
	}
	
	@Override
	public void deliver(generation.Maze mazeConfig) {
		this.Maze = mazeConfig;
	}

	public int getProgress() {
		return this.progress;
	}
	
	@Override
	public void updateProgress(int percentage) {
		this.progress = percentage;
	}
	
}
