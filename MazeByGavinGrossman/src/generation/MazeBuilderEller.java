package generation;

import java.util.Random;

/** 
 * 
 * The maze is built with a version of Eller's algorithm. 
 * 
 *   
 * @author Gavin Grossman
 */

public class MazeBuilderEller extends MazeBuilder implements Runnable {
	
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}

	/**
	 * This method generates pathways into the maze by using Eller's algorithm to randomly remove wallboards.
	 * Going row by row, we place each cell into a distinct set.
	 * We then randomly choose whether to remove the wall between them.
	 * If we do, all cells in both sets are joined into one set. 
	 * We must also randomly choose to create a vertical passage, at least one per set in each row.
	 *  
	 */
	@Override
	protected void generatePathways() {
		/**
		 * Declare two arrays of length of the width, one for left and one for right.
		 * The value of the index will tell us which set that cell is connected to.
		 * 
		 * Starting with row 0, loop over each cell, setting the value equal to index in our arrays.
		 * using randInt, remove interior walls and combine sets.
		 * at least once in every set, remove wall to the south.
		 * 
		 * for the last column, we must randomly remove southern wall separately.
		 * 
		 * create last row separately since we will not be removing any walls to the south.
		 */
		
		// doubly linked lists
		int[] left = new int[width];
        int[] right = new int[width];
        Random ran = new Random();

        // each cell is in own set
        for (int i = 0; i < width; i++) {
            left[i] = right[i] = i;
        }
     
        /* LOOPS FOR ALL MAZE CELLS */
        
        for (int y = 0; y < height-1; y++) {      	
            for (int x = 0; x < width; x++) {
            	// wallboard structures used to delete walls
            	Wallboard wallboardright = new Wallboard(x, y, CardinalDirection.East);
            	Wallboard wallboardsouth = new Wallboard(x, y, CardinalDirection.South);
            	
                /* DELETE VERTICAL WALLBOARDS */
            	
            	// if cell not in same set as cell to right or cell in a room:
                if ((floorplan.canTearDown(wallboardright) && right[x] != x + 1 && ran.nextInt(4) < 2) || floorplan.hasNoWall(x, y, CardinalDirection.East)) {
                    // links cells together then tear down the wall (may be lack of wall) 
                    left[right[x]] = left[x + 1];
                    right[left[x + 1]] = right[x];
                    left[x + 1] = x;
                    right[x] = x + 1;
                    
                    floorplan.deleteWallboard(wallboardright);     
                }
                
                /* DELETE HORIZONTAL WALLBOARDS */
                
                // randomly choose to remove lower cell from set or delete wall to the south
                if (right[x] != x && ran.nextInt(4) < 2) {
                    left[right[x]] = left[x];
                    right[left[x]] = right[x];
                    left[x] = right[x] = x;
                // if rightmost cell of set and can't go down to room:
                } else if (left[x] == x-1 && !floorplan.canTearDown(wallboardsouth)){
                	int checker = 0;
                	int w = x;
                	// see if there are any doors to room in same set
                	while (left[x] == x - 1) {
                		if (floorplan.hasNoWall(x, y, CardinalDirection.South)) {
                			checker++;
                		}
                		x--;
                	}
                	// if not, make door
                	if (checker == 0) {
                		while (left[w] == w - 1) {
                    		if (floorplan.canTearDown(wallboardsouth)) {
                    			
                    			floorplan.deleteWallboard(wallboardsouth);
                    			break;
                    		}
                    		w--;
                    	}
                	}
                // delete wall to the south 
                } else {
               
                	floorplan.deleteWallboard(wallboardsouth);
                }
            }
        }
            
        
        /* CONNECT SETS IN LAST ROW */
        
        // no need for random since we must delete walls between different sets.
        // last row so there should be a single set in the end.
        for (int x = 0; x < width-1; x++) {
            if (right[x] != x + 1) {
                left[right[x]] = left[x + 1];
                right[left[x + 1]] = right[x];
                left[x + 1] = x;
                right[x] = x + 1;
                Wallboard wallboard = new Wallboard(x, height-1, CardinalDirection.East);
                
                floorplan.deleteWallboard(wallboard);
            }
        }
        
	}

}